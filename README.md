# M5CO2: manual de operación

Consulta la versión actualizada del manual en [nuestro repositorio](https://bitbucket.org/prodimar/m5co2-manual/src/master/).

## Descripción del equipo

M5CO2 es un dispositivo de bajo consumo diseñado para monitorizar la calidad del aire en interiores, para lo que posee un sensor de CO2 por infrarrojos. Es operable por medio de su interfaz de usuario física, que incluye tres botones y una pantalla LCD. Se alimenta a través de una batería recargable por USB-C.

Cuenta con conectividad Wi-Fi; se utiliza para configurar el equipo en su interfaz web embebida, y para (opcionalmente) comunicar las lecturas realizadas a una centralita domótica, utilizando el protocolo MQTT.

## Modos de funcionamiento

El M5CO2 tiene dos modos de funcionamiento diferenciados, que afectan, entre otros, a la periodicidad de las lecturas:

- Modo de bajo consumo
- Modo continuo

### Modo de bajo consumo

Se activa cuando el dispositivo está desconectado de la alimentación USB. Busca optimizar la duración de la batería.

Está diseñado para ejecutar lecturas cada ciertos minutos (el período es configurable). Además, es posible ordenar una lectura asíncrona para ver la concentración de CO2 actual, sin tener que esperar a la siguiente lectura periódica.

Para reducir el uso de energía, el M5CO2 está inactivo y con la pantalla apagada durante la mayor parte del tiempo en este modo, activándose sólo cuando:

- Es el momento de realizar una medición periódica
- El usuario ha solicitado una medición asíncrona
- Se ha obtenido el resultado de una medición anteriormente solicitada (ver apartado "Precalentamiento").

### Modo continuo 

Se activa cuando el dispositivo está alimentado por USB. Es un modo de alto rendimiento que ejecuta lecturas frecuentes.

El modo continuo sensoriza cada segundo, la máxima frecuencia posible. En este modo se mantiene la pantalla encendida todo el tiempo.

### Precalentamiento

Antes de realizar una medición de CO2, si el equipo ha estado inactivo (porque se ha apagado, o porque está en modo de bajo consumo), es necesario realizar un precalentamiento durante 3 minutos.

Debido a esto, hay dos casos en los que hay una diferencia de tres minutos entre que se ordena una lectura y se obtiene el resultado:

- Primera lectura tras encender el dispositivo, en cualquiera de los dos modos.
- Cualquier lectura realizada en modo de bajo consumo.

## Interfaz física de usuario

### Botones

El equipo cuenta con 3 botones:

- **Botón ON/OFF**, en la esquina superior izquierda. Permite encender (pulsación corta) y apagar (pulsación larga, aproximadamente 6 segundos) el dispositivo. 

- **Botón principal** o **M5**, a la izquierda de la pantalla. Al pulsarlo, se enciende la pantalla y se ordena una nueva medida asíncrona.

- **Botón secundario**, en la esquina inferior derecha. Permite encender y apagar el pitido de aviso por concentración excesiva de CO2.

Pulsar los botones principal y secundario al mismo tiempo pone el equipo en modo de configuración (ver apartado "Portal web de configuración").

![Botones del M5CO2](img/buttons.jpeg)

### Pantalla

La interfaz gráfica tiene tres zonas diferenciadas, descritas en las siguientes secciones.

- Resultado de la última lectura
- Fecha/hora y modo
- Iconos de estado

![Interfaz gráfica del M5CO2](img/gui.jpeg)

En modo de bajo consumo, la pantalla se apaga tras un tiempo de inactividad configurable. Pulsar cualquier botón se considera actividad.

#### Resultado de la última lectura

Se muestra la concentración de CO2 sobre un fondo de color: si está por debajo de cualquier umbral el fondo será azul, si supera el umbral de aviso será amarillo y si supera el umbral de peligro será rojo. Al superar el umbral de peligro, el M5CO2 emitirá un pitido para llamar la atención del usuario. Los umbrales son configurables.

Durante el período de precalentamiento, y si se ha ejecutado al menos una medición anteriormente (en la situación del modo de bajo consumo), se muestra el último valor leído. Si es la primera medición a realizar, aparecerá el texto "Precalentando".

En caso de mostrarse una medida de CO2, sea inmediata o antigua, se indica también la hora a la que se realizó.

#### Fecha/hora y modo

En la parte inferior de la pantalla se indican la fecha y hora actuales, y el modo en el que se encuentra el dispositivo.

#### Barra de iconos

A la derecha se puede encontrar una columna de iconos.

- **Batería:** indica el nivel de batería, diferenciando entre alto, medio, bajo y crítico. Al cargar la batería, este icono reproducirá una animación de carga. Cuando se llegue al nivel máximo se sustituirá por el icono de nivel alto.

- **Conexión:** es un icono con tres posibles estados:
	- Sin conexión Wi-Fi

		![Icono de desconexión Wi-Fi](img/no_wifi.jpeg)

	- Con conexión Wi-Fi, pero sin conexión a la centralita domótica

		![Icono de conexión Wi-Fi (pero no a la centralita)](img/wifi.jpeg)

	- Con conexión a la centralita domótica

		![Icono de conexión a la centralita domótica](img/mqtt.jpeg)

- **Sonido:** indica si está habilitado el aviso sonoro de nivel excesivo de CO2 o no.

## Interfaz MQTT

Si está habilitada la integración con una centralita domótica, el M5CO2 comunicará una serie de valores a través de MQTT. Para ello, hay que configurarle los detalles de la centralita (ver sección "Parámetros de configuración").

Se propagan los siguientes valores:

- Último valor de CO2 medido, en el tópico `m5co2_<número de serie>/co2`.
- Porcentaje actual de batería, en el tópico `m5co2_<número de serie>/bat_percent`.
- Estado actual de carga, en el tópico `m5co2_<número de serie>/charging_status`. Los posibles valores son: descargando (`discharging`), cargando (`charging`) y cargado (`charged`).

## Puesta en hora

El M5CO2 utiliza la conexión Wi-Fi para ajustar la hora automáticamente en cada reinicio. Por este motivo, es bueno configurar una red Wi-Fi al menos la primera vez que se utiliza el dispositivo.

Si no se configura el uso de una centralita domótica, la conexión Wi-Fi se utilizará únicamente para la puesta en hora, desactivándose después.

## Modo de configuración

Es posible entrar al modo de configuración pulsando el botón principal y secundario al mismo tiempo. Al hacer esto, el dispositivo generará una red Wi-Fi, de nombre "m5co2_config", a la que nos podemos conectar para acceder a una página web de configuración.

Esta red puede ser abierta o protegida con contraseñas autogeneradas (configurable). En cualquiera de los dos casos, la pantalla mostrará un código QR que puede ser escaneado para obtener automáticamente las credenciales de Wi-Fi.

![QR para conectarse a la red Wi-Fi de configuración](img/qr_wifi.jpeg)

En cuanto algún dispositivo esté conectado a la red de configuración, la pantalla mostrará otro QR, que al escanearlo abre el portal web de configuración. 

![QR para acceder al portal web de configuración](img/qr_wifi.jpeg)

Es probable que el equipo usado para acceder a la configuración abra automáticamente la web tras conectarse a la Wi-Fi, o muestre un diálogo del sistema proponiéndolo. Cualquiera de los dos métodos tiene el mismo resultado.

Mientras no hay ningún dispositivo conectado a la red de configuración, el modo de configuración permanecerá activo durante 2 minutos en modo de bajo consumo y durante 10 minutos en modo continuo. Tras conectarse y cargar la web de configuración, el tiempo de actividad será de 10 minutos en ambos modos.

Es posible salir del modo de configuración en cualquier momento, pulsando el botón principal o secundario.

### Portal web

#### Página de inicio

En su página de inicio, el portal web de configuración indica la versión de firmware, la red Wi-Fi a la que está conectado y la IP (si aplica), y tres botones:

- **Configurar:** enlaza a la página de configuración de red Wi-Fi y otros parámetros.
- **Salir:** sale del portal sin guardar cambios.
- **Actualizar:** enlaza a una página en la que es posible cargar un nuevo firmware.

![Página de inicio del portal web de configuración](img/config_root.jpeg)

#### Página de configuración

Lista las redes disponibles y su cobertura, y muestra un formulario para introducir las credenciales de la red a la que nos queramos conectar. Al pulsar en cualquier red de la lista, se cargará su nombre automáticamente en el formulario.

![Configuración de la red Wi-Fi](img/config_wifi.jpeg)

Debajo del formulario de configuración de Wi-Fi, podemos ver los parámetros de configuración del M5CO2. Se detallan en la sección "Parámetros de configuración".

![Configuración del dispositivo](img/config_params.jpeg)

Es importante notar que el parámetro "Usar centralita domótica" hace que se muestren u oculten los parámetros relacionados con la conexión a la centralita.

Debajo del formulario hay 3 botones:

- **Guardar:** guarda los valores introducidos en memoria persistente y cierra el portal de configuración.
- **Recargar redes:** recarga la lista de redes Wi-Fi disponibles.
- **Restaurar valores y salir:** borra la configuración, restaurando los valores por defecto. Cierra el portal de configuración.

![Botones de la página de configuración](img/config_save.jpeg)

Después de cerrar el portal de configuración, el dispositivo se reiniciará para aplicar los nuevos valores de los parámetros.

### Parámetros de configuración

#### Generales

| Nombre                     | Función                                                                            | Posibles valores                  | Valor por defecto             |
| :------------------------: | :--------------------------------------------------------------------------------- | :-------------------------------- | :---------------------------: |
| Número de serie            | Identifica al M5CO2 en la comunicación MQTT.                                       | No aplica; trae por defecto un número de serie único configurado. |
| Sonido                     | Activar o desactivar el sonido.                                                    | On / Off.                         | On                            |
| Período medidas            | Cada cuanto ejecutar una medida en modo de bajo consumo.                           | Enteros entre 1 y 59 (minutos).   | 15 minutos                    |
| Tiempo espera pantalla     | Cuánto tiempo permanece la pantalla encendida en modo de bajo consumo.             | Enteros entre 1 y 900 (segundos). | 8 segundos                    |
| Umbral CO2 peligro         | Umbral de CO2 por encima del cual se activa el aviso sonoro y la pantalla en rojo. | Enteros entre 400 y 5000 (ppm).   | 1200 ppm                      |
| Umbral CO2 aviso           | Umbral de CO2 por encima del cual se activa la pantalla en amarillo.               | Enteros entre 400 y 5000 (ppm).   | 800 ppm                       |
| Usar centralita domótica   | Activar o desactivar la comunicación MQTT (y mostrar o no los parámetros MQTT).    | On / Off.                         | Off                           |
| Claves Wi-Fi autogeneradas | Activar o desactivar la seguridad Wi-Fi para la red de configuración.              | On / Off.                         | Off                           |

#### MQTT

| Nombre                     | Función                                       | Posibles valores                             | Valor por defecto |
| :------------------------: | :-------------------------------------------- | :------------------------------------------- | :---------------: |
| Puerto MQTT      | Puerto configurado para MQTT en la centralita domótica. | Enteros entre 1024 y 65535.                  | 1883              |
| IP servidor MQTT | IP de la centralita domótica.                           | IPv4 en formato `x.x.x.x`.                   | Ninguno           |
| Usuario MQTT     | Usuario MQTT configurado en la centralita domótica.     | Palabra alfanumérica de hasta 50 caracteres. | Ninguno           |
| Clave MQTT       | Clave MQTT configurada en la centralita domótica.       | Palabra alfanumérica de hasta 50 caracteres. | Ninguno           |

### Actualización de firmware

La actualización de firmware sin cables es una funcionalidad disponible a partir de la versión de firmware 1.0.4.

La página de actualización de firmware tiene el siguiente aspecto.

![Página de actualización de firmware](img/config_update.jpeg)

Al pulsar en el botón "Seleccionar archivo", se abrirá un diálogo del sistema para seleccionar el binario de firmware correspondiente. Si hemos entrado en el portal de configuración a través de una vista rápida y no del navegador, es posible que no se cargue bien el diálogo ya que esa vista no tiene los permisos adecuados.

Tras indicar el binario, hay que pulsar en el botón "Actualizar". Dependiendo del navegador, podrá mostrar una barra de progreso, como si estuviera cargando una nueva página. Actualmente, las actualizaciones tardan unos segundos en completarse. Tras su finalización, la web mostrará un mensaje de éxito o fracaso. En cualquier caso, el dispositivo se reiniciará después.

![Actualización con éxito](img/config_update_ok.jpeg)
![Actualización fallida](img/config_update_fail.jpeg)

La actualización de firmware no invalida la configuración guardada.